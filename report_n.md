# Entrega dos exercícios

- **Grupo**: ds122-2022-2-n
- **Última atualização**: sex 17 fev 2023 10:47:07 -03

|Nome|	ds122-prepare-assignment<br>2023-03-14|	ds122-http-assignment<br>2023-03-14|	ds122-html-tables-assignment<br>2023-03-14|	ds122-html-store-assignment<br>2023-03-14|	ds122-css-assignment<br>2023-03-14|	ds122-js-exercises-assignment<br>2023-03-14|	ds122-dom-assignment<br>2023-03-14|	ds122-php-assignment<br>2023-03-14|	ds122-php-form-assignment<br>2023-03-14|
|----|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|
|ADRIANO_MONTAGUTI|	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ALINE_MACHADO_LIMA|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ANDERSON_SOUZA_SANTOS|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ANDRÉ_ANASTÁCIO_DE_OLIVEIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|ANDRÉ_LUIZ_LEME|	 ok |	 Fork não encontrado |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |
|BRUNO_CARLOS_DE_SOUZA_BLETES|	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|CAIO_EDUARDO_MOREIRA_DE_LIMA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|CAROLINA_DOS_SANTOS_DE_LIMA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|CAROLINE_PICANCO_PROCKMANN|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |
|CRISTIANO_MARTINS_DE_SOUZA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|DIEGO_ALVES_LISBOA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|DOUGLAS_TIAGO_VALÉRIO|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|EDUARDO_COSTA_MATOS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|EDUARDO_VINICIUS_FROHLICH|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|ESDRAS_EMANUEL_ALVES_BEZERRA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|EVERTON_DA_SILVA_VIEIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|FLÁVIO_LUÍS_MOCHINSKI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork não encontrado |
|GABRIEL_FELIPE_BATISTA_DOS_SANTOS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GABRIEL_HENRIQUE_DE_ASSIS_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|GABRIEL_HISSATOMI_DAVANZO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|
|GABRIELA_SCHNEIDER_LOPES|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork não encontrado |
|GUSTAVO_ANTONIO_PADILHA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GUSTAVO_IASSYL_NIELSEN_ALVES|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GUSTAVO_JAHNZ|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|HENRIQUE_ULBRICH_DE_SOUZA|	 Fork não encontrado |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|JAVAN_MACHADO_BONFIM|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|JOÃO_GUILHERME_ALMEIDA_COUTO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|JORGE_VICTOR_BENCKE_GASTALDI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|LAISA_CRISTINA_KROLIKOVSKI_DA_SILVA|	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LEONARDO_DE_LIMA_PEREIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|LUCAS_DOS_SANTOS_SANTANA_PINTO|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LUCAS_FRACARO_NUNES|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LUCAS_HENRIQUE_RAMOS_CAZIONATO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LUCAS_VINICIUS_DE_CARVALHO|	 ok |	 Fork não encontrado |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|LUIS_FERNANDO_TUROZI_MAUSSON|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MARCEL_ALESSANDRO_ZIMMER|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |
|MARCEL_RODRIGUES_CARDOZO|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MARIANE_ROESLER|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|MARICOT_NICOLAS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|
|MATHEUS_DIONYSIO_CLASSE|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |
|MATHEUS_HENRIQUE_MIRANDA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|MATHEUS_RIGLER|	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|PABLO_ALVES_FERNANDES|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|RAPHAEL_PERES_DE_OLIVEIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|RODRIGO_BARBOSA_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RUAN_SERGIO_CUNHA_BRITO|	 Fork não encontrado |	 Fork não encontrado |	 ok |	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |	 ok |
|RUPÉLIO_COLFERAI_JUNIOR|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |
|VICTORIA_KUNZ_VIEIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|VINÍCIUS_ALVES_DOS_SANTOS|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|VINICIUS_GUSTAVO_RAZOTO|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 Fork não encontrado |	 ok |	 ok |	 Fork não encontrado |
|VITÓRIA_LAÍS_SOUZA_DOS_SANTOS|	 ok |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|WILLIAM_MATHEUS_DEL_PONTE_FERREIRA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|YASMIN_TAINÁ_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
